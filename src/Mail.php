<?php

	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	namespace Ikaros;

	class Mail extends PHPMailer {

		private $recipients;
		private $subject;
		private $body;
		private $headers;

		function __construct () {
			parent::__construct(true);
			$this -> headers = [];
	    }

	    public function recipient (string $email, ?string $name): void {
		    $this -> addAddress ($email, $name);
	    }

	    public function header (string $header): void {
		    if (!in_array ($header, $this -> headers)) {
			    array_push ($this -> headers, $header . "\r\n");
		    }
		}

		public function from (string $email, ?string $name) {
			$this -> setFrom ($email, $name);
		}

		public function replyTo (string $email, ?string $name) {
			$this -> addReplyTo ($email, $name);
		}

	    public function subject (string $subject): void {
		    $this -> Subject = $subject;
		}

		public function cc (string $email) {
			$this -> addCC ($email);
		}

		public function bcc (string $email) {
			$this -> addBCC ($email);
		}

	    public function body (string $body): void {
		    $this -> Body = $body;
		}

		public function smtp (string $host, string $port) {
			$this -> isSMTP ();
			$this -> SMTPAuth = true;
			$this -> SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
			$this -> Host = $host;
			$this -> Port = $port;
		}

		public function authentication (string $username, string $password) {
			$this -> Username = $username;
			$this -> Password = $password;
		}
	}
?>