<?php

	namespace Ikaros;

	class Router {

		/**
		 * @var string $domain Router domain
		 */
		private static $_domain;

		/**
		 * @var array $routes Collection of registered routes, categorized by
		 * their request method.
		 */
		private static $routes = [
				'ANY' => [],
				'GET' => [],
				'POST' => [],
				'PUT' => [],
				'DELETE' => [],
				'PATCH' => [],
				'OPTIONS' => []
			];

		/**
		 * Register a route for access via GET method.
		 *
		 * @param string $route Route pattern To Match
		 * @param callable $action Callback function to run when accessed
		 *
		 * @return void
		 */
		public static function get (string $route, callable $action): void {
			self::register ('GET', new Route ($route, $action));
		}

		/**
		 * Register a route for access via POST method.
		 *
		 * @param string $route Route pattern To Match
		 * @param callable $action Callback function to run when accessed
		 *
		 * @return void
		 */
		public static function post (string $route, callable $action): void {
			self::register ('POST', new Route ($route, $action));
		}

		/**
		 * Register a route for access via PUT method.
		 *
		 * @param string $route Route pattern To Match
		 * @param callable $action Callback function to run when accessed
		 *
		 * @return void
		 */
		public static function put (string $route, callable $action): void {
			self::register ('PUT', new Route ($route, $action));
			self::register ('OPTIONS', new Route ($route, $action));
		}

		/**
		 * Register a route for access via PATCH method.
		 *
		 * @param string $route Route pattern To Match
		 * @param callable $action Callback function to run when accessed
		 *
		 * @return void
		 */
		public static function patch (string $route, callable $action): void {
			self::register ('PATCH', new Route ($route, $action));
			self::register ('OPTIONS', new Route ($route, $action));
		}

		/**
		 * Register a route for access via DELETE method.
		 *
		 * @param string $route Route pattern To Match
		 * @param callable $action Callback function to run when accessed
		 *
		 * @return void
		 */
		public static function delete (string $route, callable $action): void {
			self::register ('DELETE', new Route ($route, $action));
			self::register ('OPTIONS', new Route ($route, $action));
		}

		/**
		 * Register a route for access via OPTIONS method.
		 *
		 * @param string $route Route pattern To Match
		 * @param callable $action Callback function to run when accessed
		 *
		 * @return void
		 */
		public static function options (string $route, callable $action): void {
			self::register ('OPTIONS', new Route ($route, $action));
		}

		/**
		 * Register a route for access via any method.
		 *
		 * @param string $route Route pattern To Match
		 * @param callable $action Callback function to run when accessed
		 *
		 * @return void
		 */
		public static function any (string $route, callable $action): void {
			self::register ('ANY', new Route ($route, $action));
			self::register ('OPTIONS', new Route ($route, $action));
		}

		/**
		 * The router will find the required route by the specified route pattern.
		 * This method should be used inside the callback function of a route
		 * if running another route is required.
		 *
		 * @param string $route Pattern of the route to find
		 *
		 * @return Route
		 */
		public static function use (string $route): Route {
			return self::find ($_SERVER['REQUEST_METHOD'], $route);
		}

		/**
		 * Redirect the user to a specific path or URL
		 *
		 * @param string $route The route or path to redirect to
		 * @param bool $external Whether the route represents an internal path
		 * or an external URL
		 *
		 * @return void
		 */
		public static function redirect (string $route, bool $external = false): void {
			if ($external === true) {
				header ('Location: ' . $route);
			} else {
				header ('Location: ' . rtrim (self::domain (), '/') . $route);
			}
		}

		/**
		 * Register a route
		 *
		 * @param string $method Method used by the route
		 * @param callable $route Route object to add
		 *
		 * @return void
		 */
		private static function register (string $method, Route $route): void {
			array_push (self::$routes[$method], $route);
		}

		/**
		 * Find a registered route
		 *
		 * The function will loop over all the collection of routes with the
		 * given method and the any method and checks if it matches the given
		 * route.
		 *
		 * @param string $method Method used by the route to find
		 * @param string $route Route pattern to find
		 *
		 * @return Route|null The Route object or null if not found
		 */
		private static function find (string $method, string $route): ?Route {
			$routes = array_merge (self::$routes[$method], self::$routes['ANY']);
			foreach ($routes as $registered) {
				if ($registered -> match ($route)) {
					return $registered;
				}
			}
			return null;
		}

		/**
		 * Get only route portion of the currently accesed url, removing the script
		 * that's being accessed as well as the domain and protocol.
		 *
		 * @return string
		 */
		public static function route (): string {
			$basepath = implode ('/', array_slice (explode ('/', $_SERVER['SCRIPT_NAME']), 0, -1)) . '/';
	    	$url = substr ($_SERVER['REQUEST_URI'], strlen ($basepath));

			if (strstr ($url, '?')) {
		    	$url = substr ($url, 0, strpos ($url, '?'));
			}

	    	$url = '/' . trim ($url, '/');
	    	$url = str_replace ('index.php', '', $url);
	    	return $url;
		}


		/**
		 * Get the router domain, including the protocol and a trailing slash
		 *
		 * @return string|void
		 */
		public static function domain (string $domain = null): string {
			if (is_string ($domain)) {
				self::$_domain = $domain;
			}
			return self::protocol () . self::$_domain . '/';
		}

		/**
		 * Returns the protocol on which the website is running.
		 *
		 * i.e. https:// or http://
		 *
		 * @return string
		 */
		public static function protocol (): string {
			if (self::https ()) {
				return 'https://';
			} else {
				return 'http://';
			}
		}

		/**
		 * Returns the full current URL, including the protocol, domain and route
		 *
		 * @return string
		 */
		public static function fullRoute (): string {
			return rtrim (self::domain (), '/') . self::route ();
		}

		/**
		 * Makes the Router listen for any requests and run the callback function
		 * if a route is accessed.
		 *
		 * @param Route $route - The route to access to. If none is defined, the
		 * route will be automatically detected from the request. (default: null)
		 *
		 * @return void
		 */
		public static function listen (Route $route = null): void {

			if ($route !== null) {
				$found_route = $route;
			} else {
				$found_route = self::find ($_SERVER['REQUEST_METHOD'], self::route ());
			}


			if ($found_route !== null) {
				$content = $found_route -> run ();
				if (is_array ($content)) {
					if (HTTP::type () === null) {
						HTTP::type ('json');
					}
					echo new JSON ($content);
				} else if ($content instanceof Route) {
					static::listen ($content);
				} else {
					if (HTTP::type () === null) {
						HTTP::type ('html');
					}
					echo $content;
				}
			} else {
				HTTP::error (404);
			}
		}

		/**
		 * Check if the application is being served via HTTPS
		 *
		 * @return bool
		 */
		private static function https (): bool {
			return (!empty ($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443));
		}

	}
?>