<?php
	namespace Ikaros;

	use Session;
	use HTTP;
	use Token;
	use Password;

	use PragmaRX\Google2FA\Google2FA;

	class Authenticator {

		static function authenticate (string $method = 'session', ?string $value) : void {
			try {
				$authenticated = false;
				switch ($method) {
					case 'session':
						$authenticated = Session::active ();
						break;

					case 'token':
						$authenticated = Token::read ($value, 15 * 24 * 60 * 60);
						break;
				}

				if ($authenticated === false) {
					HTTP::error (401);
				}
			} catch (Exception $e) {
				HTTP::error (401);
			}
		}

		static function login (string $method = 'session', string $password, string $hash, any $user)  {
			if (Password::compare ($password, $hash)) {
				switch ($method) {
					case 'session':
						Session::active (true);
						Session::user ($user);
						return true;
						break;

					case 'token':
						$token = Token::create ([
							"user" => $user,
							"active" => true
						]);
						return $token;
						break;
				}
			}

			return false;
		}

		static function logout (string $method = 'session', any $value = null) {
			switch ($method) {
				case 'session':
					Session::end ();
					return true;
					break;

				case 'token':
					$token = Token::payload ($value, ["active" => false]);
					return $token;
					break;
			}
		}
	}
?>