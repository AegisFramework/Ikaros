<?php

	namespace Ikaros;

    class HTTP {

		public static $contentType;

        /**
         * Set the response type header
         *
         * @param string $type Content type ('json', 'html', 'xml', )
         * @param string $charset Charset for response (default: utf-8)
         */
        public static function type (string $type = null, string $charset = 'utf-8') {
			if ($type !== null) {
				self::$contentType = $type;
				switch($type){
					case 'json':
	                    header ("Content-Type: application/json;charset=$charset");
	                    break;

	                case 'html':
	                    header ("Content-Type: text/html;charset=$charset");
						break;

					case 'xml':
						header ("Content-Type: application/xml;charset=$charset");
						break;

					case 'text':
						header ("Content-Type: text/plain;charset=$charset");
						break;
	            }
			} else {
				return self::$contentType;
			}
        }

		public static function allow ($methods) {
			header("Access-Control-Allow-Methods: " . implode(", ", $methods));
		}

		public static function whitelist ($domain) {
			header("Access-Control-Allow-Origin: $domain");
		}

		public static function credentials ($bool) {
			if ($bool) {
				header('Access-Control-Allow-Credentials: true');
			} else {
				header('Access-Control-Allow-Credentials: false');
			}
		}

        /**
         * Send error response
         *
         * Set the error header to the response and build it's custom error
         * page adding the debug information if it's enabled. After printing
         * the page it dies.
         *
         * @param int $code | HTTP Error Code
         * @param int $number | PHP Error Number
         * @param string $message | Error Description
         * @param string $file | File In Which The Error Is
         * @param int $line | Line Number
         *
         */
        public static function error ($code, $number = null, $message = null, $file = null, $line = null) {
			switch ($code){
                case 400:
					header($_SERVER["SERVER_PROTOCOL"]." 400 Bad Request", true, 400);
					$title = "Bad Request";
					$errorDescription = "The request is invalid.";
					break;

				case 401:
					header($_SERVER["SERVER_PROTOCOL"]." 401 Unauthorized", true, 401);
					$title = "Unauthorized Access";
					$errorDescription = "Autentication is Required.";
					break;

				case 403:
					header($_SERVER["SERVER_PROTOCOL"]." 403 Forbidden", true, 403);
					$title = "Forbidden";
					$errorDescription = "Forbidden access, clearance neeeded.";
					break;

				case 404:
					header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
					$title = "Page Not Found";
					$errorDescription = "Sorry, the page you are trying to access does not exist.";
					break;

                case 409:
					header($_SERVER["SERVER_PROTOCOL"]." 409 Conflict", true, 409);
					$title = "Conflict";
					$errorDescription = "A request or file conflict ocurred, please try again.";
					break;

				case 500:
					header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error", true, 500);
					$title = "Server Error";
					$errorDescription = "Sorry, it seems there's been an error. Please try later.";
					break;
			}

			if (Debug::level () >= Debug::ERROR) {
				if ($message === null && $errorDescription !== null) {
					$message = $errorDescription;
				}

				Debug::error ($code, $title, $errorDescription, $message, [], $file, $line);
			} else {
				$error = new Template ();
				$error -> setContent (file_get_contents ("error/$code.html"));
				$error -> compile ();
				echo $error;
				die ();
			}

        }
    }

?>