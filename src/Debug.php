<?php

	namespace Ikaros;

	class Debug {

		const NONE = 0;
		const ERROR = 1;
		const WARNING = 2;
		const INFO = 3;
		const ALL = 4;

		private static $_level;

		public static function level (int $level = null) {
			if ($level !== null) {
				self::$_level = $level;
			} else {
				return self::$_level;
			}
		}

		public static function init (int $level = self::NONE) {

			self::level ($level);

			// Set custom exception handler function
			set_exception_handler(function ($exception) {
				HTTP::error (500, $exception -> getCode (), $exception -> getMessage (), $exception -> getFile (), $exception -> getLine ());
				return true;
			});

			// Set custom error handler function
			set_error_handler(function ($errorNumber, $errorString, $errorFile, $errorLine) {
				if (!(error_reporting () && $errorNumber)) {
					return false;
				}
				HTTP::error (500, $errorNumber, $errorString, $errorFile, $errorLine);
				return true;
			});

			// Set custom fatal error handler function
			register_shutdown_function (function () {
				$error = error_get_last ();
				if ($error['type']) {
					HTTP::error (500, $error['type'], $error['message'], $error['file'], $error['line']);
					return true;
				}
			});

			// Turn off default error reporting
			error_reporting(0);
		}

		public static function error (int $code, ?string $title = 'Debugging Log', ?string $description = 'Sorry, an error has ocurred.', ?string $message = 'An error has ocurred, check the log to see what’s going on.', ?array $log = [], ?string $file = null, ?int $line = null): void {
			if (self::level () >= self::ERROR) {
				$object = [
					'OS' => PHP_OS,
					'PHP Version' => PHP_VERSION,
					'Message' => $message
				];

				if ($file !== null) {
					$object['File'] = $file;
					$object['Line'] = $line;
				} else {
					$object['File'] = debug_backtrace()[0]['file'];
					$object['Line'] = debug_backtrace()[0]['line'];
				}

				$object = array_merge ($object, $log);
				if (HTTP::$contentType === 'json') {
					echo new JSON ($object);
				} else {
					$error = new Template ('error.html');

					$error -> data['title'] = $title;
					$error -> data['message'] = $description;

					$error -> data['description'] = '';
					foreach ($object as $key => $value) {
						$error -> data['description'] .= "<p><b>$key:</b> $value</p>";
					}
					$error -> data['description'] = '<div>'.$error -> data['description'].'</div>';
					$error -> compile ();
					echo $error;
				}
			}
			die();
		}
	}

?>