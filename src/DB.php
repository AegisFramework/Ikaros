<?php

	namespace Ikaros;

	use PDO;
	use PDOException;
	use Exception;

	class DB {

		private static $charset;
		private static $database;
		private static $host;
		private static $pass;
		private static $connection;
		private static $user;

		public static function connect (string $user, string $pass, string $database, string $host = 'localhost', string $charset = 'utf8'): void {
			try {
				self::$host = $host;
			    self::$user = $user;
			    self::$pass = $pass;
			    self::$database = $database;
				self::$charset = $charset;
			    self::$connection = new PDO("mysql:host=$host;dbname=$database;charset=$charset", $user, $pass);

				Schema::initialize ();

			} catch (PDOException $e) {
				throw new Exception('Connection failed, check your credentials and user permissions.', 1);
			}
		}

		public static function isEmpty (): bool {
			return count (self::tables ()) == 0;
		}

		public static function connection (): PDO {
		    return self::$connection;
		}

		public static function last (): int {
			return self::$connection -> lastInsertId ();
		}

		public static function name (): string {
			return self::$database;
		}

		private static function prepare ($query) {
			$sth = self::connection () -> prepare ($query);
			if ($sth) {
				return $sth;
			} else {
				throw new Exception("Error preparing query.<p><b>Query:</b> " . $sth -> queryString . "</p><p><b>Database Error:</b> ". $sth -> errorInfo ()[2]. "</p>", 1);
			}
		}

		private static function execute (&$sth, $array) {
			if (!$sth -> execute ($array)) {
				throw new Exception("Error executing query.<p><b>Query: </b>" . $sth -> queryString. "</p><p><b>Database Error:</b> ". $sth -> errorInfo ()[2]. "</p>", 1);
			}
		}

		public static function query ($query, $array = []){
			$sth = self::prepare ($query);
			self::execute ($sth, $array);
			return $sth;
		 }

		public static function attribute ($attribute) {
			return self::connection () -> getAttribute($attribute);
		}

		public static function tables () {
			$tables = [];

			$query = self::query ("SELECT DISTINCT `table_name` FROM `information_schema`.`columns` WHERE `table_schema` = '" . self::$database . "'") -> fetchAll (PDO::FETCH_ASSOC);

			foreach ($query as $key => $value) {
				array_push ($tables, $value['table_name']);
			}

			return $tables;
		}

		public static function restore ($file) {
			$backup = FileSystem::read ($file);
			self::query ($backup);
		}

		public static function backup ($directory, $filename = null, $structure = true, $content = true) {
			$date = date ('Y-m-d_h_i_s');
			if ($filename === null) {
				$filename = self::$database . '_backup_'. $date . '.sql';
			}

			$file = "$directory/$filename";
			$database = self::$database;
			$destructive = false;

			if (FileSystem::exists ($file)) {
				$destructive = true;
			}

			FileSystem::write ($file, "-- Aegis Database Backup\n", $destructive);
			FileSystem::write ($file, "-- Server version: " . self::attribute (PDO::ATTR_SERVER_VERSION) . "\n");
			FileSystem::write ($file, "-- Generation Date: $date\n");
			FileSystem::write ($file, "-- PHP version: " . phpversion () . "\n");
			FileSystem::write ($file, "-- Database: " . self::name () . "\n");

			$tables = self::tables ();

			FileSystem::write ($file, "SET FOREIGN_KEY_CHECKS=0;\n");

			foreach ($tables as $table) {

				if ($structure === true) {
					FileSystem::write ($file, "-- ============================== \n");
					FileSystem::write ($file, "-- Structure for $table\n");
					FileSystem::write ($file, "-- ============================== \n\n");

					FileSystem::write ($file, "DROP TABLE IF EXISTS `$table`;");

					if ($create = self::query ("SHOW CREATE TABLE $table")) {
						$row_create = $create -> fetch (PDO::FETCH_ASSOC);
						FileSystem::write ($file, "\n" . $row_create['Create Table'] . ";\n");
					}
				}

				if ($content === true) {
					FileSystem::write ($file, "-- ============================== \n");
					FileSystem::write ($file, "-- Dump Data for `$table`\n");
					FileSystem::write ($file, "-- ============================== \n\n");

					$query = self::query ("SELECT * FROM $table");

					$fields = '';

					while ($record = $query -> fetch (PDO::FETCH_ASSOC)) {
						if ($fields === '') {
							$fields = '`' . \implode ('`, `', \array_keys ($record)) . '`';
						}

						$values = \implode (', ', array_map (function ($value) {
							return "'". str_replace ("'", "''", $value) . "'";
						}, array_values ($record)));

						FileSystem::write ($file, "INSERT INTO `$table` ($fields) VALUES ($values);\n");
					}
				}
			}

			FileSystem::write ($file, "SET FOREIGN_KEY_CHECKS=1;\n");
		}

		function __destruct() {
		    self::$connection = null;
		}
	}

?>