<?php

	namespace Ikaros;

	class Util {

		/**
		 * Generate a v4 compilant UUID
		 *
		 * This function will use the random_bytes () function, assuring the
		 * randomness of the UUID to generate.
		 *
		 * @return string Generated Random UUID
		 */
		public static function uuid (): string {
			$bytes = random_bytes (16);
			$bytes[6] = chr ((ord ($bytes[6]) & 0x0f) | 0x40);
			$bytes[8] = chr ((ord ($bytes[8]) & 0x3f) | 0x80);
			return vsprintf ('%s%s-%s-%s-%s-%s%s%s', str_split (bin2hex ($bytes), 4));
		}
	}

?>