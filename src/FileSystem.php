<?php

	namespace Ikaros;

	use RecursiveIteratorIterator;
	use RecursiveDirectoryIterator;
	use Exception;
	use RuntimeException;

	class FileSystem {

		private static $_root = '';

		/**
		 * Set the Root directory of your project
		 *
		 * @param string|null $directory The path to your root directory, usually __DIR__
		 *
		 * @return string|void
		 */
		public static function root ($directory = null) {
			if ($directory !== null) {
				self::$_root = $directory;

				// Now that the root has been defined, we can set up the autoloader
				// for both classes inside the lib directory and components.
				spl_autoload_register (function ($className) {
					$file = self::findFile ('lib', "$className.php");
					if ($file !== null) {
						require_once ($file);
					} else {
						$directory = self::findDirectory ('lib/components', $className);
						if ($directory !== null) {
							require_once ("$directory/index.php");
						}
					}
				});
			} else {
				return self::$_root;
			}
		}

		/**
		 * Recursively search for a file inside a directory
		 *
		 * Searches for a file by it's name inside a directory in a recursive
		 * way, which will return the full path to it if found.
		 *
		 * @param string $directory - Directory in which the search will begin
		 * @param string $file - Name of the file to find
		 *
		 * @return string|null - String of the path to the file or null
		 * if it's not found.
		 */
		public static function findFile (string $directory, ?string $file): ?string {
			$objects = self::listFiles ($directory);
			foreach ($objects as $name => $object) {
				if ($object -> getFileName () == $file) {
					return $name;
				}
			}
			return null;
		}

		public static function findDirectory (string $directory, ?string $dir): ?string {
			$objects = self::listFiles ($directory);
			foreach ($objects as $name => $object) {
				if ($object -> isDir ()) {
					if ($object -> getFileName () == $dir) {
						return $name;
					}
				}
			}
			return null;
		}

		/**
		 * List all the files on a directory
		 *
		 * The listing will be performed in a recursive way, including all the
		 * subdirectories of the given directory.
		 *
		 * @param string $directory - Directory to list the files for
		 *
		 * @throws Exception - If directory has no read permissions. 0744 should work.
		 *
		 * @return array - List of all the files contained in the directory
		 */
		public static function listFiles (string $directory): iterable {

			if (self::readable ($directory)) {
				return new RecursiveIteratorIterator (new RecursiveDirectoryIterator (self::root () . '/' . $directory), RecursiveIteratorIterator::SELF_FIRST);
			} else {
				throw new Exception ("The directory '$directory' is not readable. Listing files is not possible.", 1);
			}
		}

		/**
		 * Get the contents of a file
		 *
		 * @param string $file - Path To file to read
		 *
		 * @return string - File Contents
		 */
		public static function read (string $file): string {
			return file_get_contents (self::root () . '/' . $file);
		}

		/**
		 * Check if a directory has writable permissions.
		 *
		 * This will first check if the directory can be read. If it can be read,
		 * the write permission will be checked.
		 *
		 * @param string $path - The path to the directory to perform the check
		 * on
		 *
		 * @return boolean
		 */
		public static function writable (string $path): bool {
			if (self::readable ($path)) {
				return is_writable (self::root () . '/' . $path);
			}
			return false;
		}

		/**
		 * Check if a file or directory has read permissions
		 *
		 * @param string $path - Path to the file or directory to perform the
		 * check on
		 *
		 * @return boolean
		 */
		public static function readable (string $path): bool {
			return is_readable (self::root () . '/' . $path);
		}

		/**
		 * Check if a file exists
		 *
		 * @param string $file - Path To file
		 *
		 * @return boolean
		 */
		public static function exists (string $file): bool {
			return file_exists (self::root () . '/' . $file);
		}

		/**
		 * Write contents to a file
		 *
		 * @param string $file - Path To File
		 * @param string $content - Content To Write
		 * @param bool $destructive - Whether contents should be appended to the
		 * file or it should replace whatever the file currently holds
		 */
		public static function write (string $file, string $content, bool $destructive = false) {
			$destination = self::root () . '/' . $file;
			$directory = \dirname ($file);

			if (self::writable ($directory)) {
				$flag = 'a+';
				if ($destructive === true) {
					$flag = 'w+';
				}
				$resource = fopen ($destination, $flag);
				fwrite ($resource, $content);
				fclose ($resource);
			} else {
				$filename = \basename ($destination);
				throw new RuntimeException ("Unable to write file to destination directory, write permissions missing<p><b>Directory:</b> $directory</p><p><b>Filename:</b> $filename</p>", 1);
			}
		}
	}
?>