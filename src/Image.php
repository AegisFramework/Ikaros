<?php

	namespace Ikaros;

	use ImageJPEG;
	use ImageDestroy;

	class Image {

		/**
		 * Fix the orientation of an image. On some devices such as iPads and
		 * iPhones, uploaded images may appear to be upside down. Calling this
		 * function will fix its orientation.
		 *
		 * This is only necessary JPEG images.
		 *
		 * @param string $file Path to the file
		 *
		 * @return void
		 */
		public static function fixOrientation (string $file) {
	        if (exif_imagetype ($file) == 2) {
                $exif = exif_read_data ($file);
            	if (array_key_exists ('Orientation', $exif)) {
                	$orientation = $exif['Orientation'];
                    $images_orig = ImageCreateFromJPEG ($file);
                    $rotate = '';
					switch ($orientation) {
					   case 3:
					      $rotate = imagerotate ($images_orig, 180, 0);
					      break;
					   case 6:
					      $rotate = imagerotate ($images_orig, -90, 0);
					      break;
					   case 8:
					      $rotate = imagerotate ($images_orig, 90, 0);
					      break;
					}

					if ($rotate != '') {
	                    ImageJPEG ($rotate, $file);
	                    ImageDestroy ($rotate);
					}
					ImageDestroy ($images_orig);
            	}
            }
        }
	}

?>