<?php

	namespace Ikaros;

	use Request;
	use Router;

	use Exception;

	use Branca\Branca;

	class Token {

		protected static $key;

		static function key (string $key) : void {
			static::$key = $key;
		}

		static function read (string $token, ?int $ttl = null) : bool {
			try {
				$branca = new Branca (static::$key);

				$content = null;

				if ($ttl !== null) {
					$content = $branca -> decode ($payload, $ttl);
				} else {
					$content = $branca -> decode ($payload);
				}

				return new Collection ($content);
			} catch (RuntimeException $exception) {
				throw new Exception ($exception -> getMessage (), 1);
			}
		}

		static function create (array $payload = []) : string {
			$branca = new Branca (static::$key);

			$iat = time (); // time of token issued at
			$nbf = $iat; //not before in seconds
			$exp = $iat + (15 * 24 * 60 * 60); // expire time of token in seconds

			$domain = rtrim (Router::domain (), '/');

			$payload = array_merge ([
				"iss" => $domain,
				"aud" => $domain,
				"iat" => $iat,
				"nbf" => $nbf
			], $payload);

			return $branca -> encode (json_encode ($payload));
		}

		static function payload (string $token, any $payload = null) {
			if ($payload !== null) {
				$previousPayload = static::read ($token);

				return static::create (array_merge ([
					$previousPayload,
					$payload
				]));
			} else {
				return static::read ($token);
			}
		}
	}
?>