<?php

	namespace Ikaros;

	use Exception;

	class Template {

		protected $_page;
		protected $_template;
		protected $_content;
		protected $_title;
		protected $_keywords;
		protected $_description;
		protected $_twitter;
		protected $_google;
		protected $_shareimage;
		protected $_author;
		protected $_domain;
		protected $_route;
		protected $_fullRoute;
		protected $_scripts = ['./js/index.js'];
		protected $_styles = ['./style/index.css'];
		protected $_index;
		protected $_count;

		function __construct ($template = null) {
			$this -> template ($template);
		}

		public $data = [];

		/**
		 * Set the page to use for this template as base
		 *
		 * @param string $page | Page Name and Extension
		 */
		public function page ($page){
			$this -> _page = FileSystem::findFile ('lib/components', $page);
			$this -> compileContent();
		}

		public function styles (array $styles): void {
			$this -> _styles = $styles;
		}

		public function scripts (array $scripts): void {
			$this -> _scripts = $scripts;
		}

		/**
		 * Set the template file for this template
		 *
		 * @param string $template | Template Name and Extension
		 */
		public function template ($template = null) {
			$directory = null;
			if (get_class ($this) !== 'Ikaros\\Template') {
				$directory = FileSystem::findDirectory ('lib/components', get_class ($this));
			}
			if ($directory !== null) {
				$this -> _template = "$directory/index.html";
			} else if ($template !== null) {
				$this -> _template = FileSystem::findFile ('lib/components', $template);
			}

			$this -> compileContent();
		}

		protected function compileContent(){
			if (is_string($this -> _page)) {
				$this -> _content = file_get_contents($this -> _page);
				if(is_string($this -> _template)){
					$this -> _content = str_replace("{>{content}<}", file_get_contents($this -> _template), $this -> _content);
				}
			}else{
				if(is_string($this -> _template)){
					$this -> _content = file_get_contents($this -> _template);
				}
			}
		}

		public function setContent($content){
			$this -> _content = $content;
		}

		public function compile () {
			$this -> evaluateConditionals ();
			$this -> linkFiles ();
			$this -> setProperties ();
			$this -> includeTemplates ();
			$this -> setProperties ();
			$this -> includeRepeats ();
			$this -> setProperties ();
		}

		protected function evaluateIfElse () {
			preg_match_all ('/(\{\{if\s(\w+)\}\})(.*)(\{\{else\}\})(.*)\{\{\/if\}\}/sU', $this -> _content, $matches);
			if (!empty(array_filter($matches))) {
				$variable = $matches[2][0];
				$trueBlock = $matches[3][0];
				$falseBlock = $matches[5][0];

				$value;
				$content;

				if (property_exists ($this, $variable)) {
					$value = $this -> $variable;
				}else if (array_key_exists ($variable, $this -> data)) {
					$value = $this -> data[$variable];
				} else if (method_exists($this, $variable)) {
					$value = $this -> $variable ();
				}

				if (isset ($value)) {
					if ($value === true) {
						$content = $trueBlock;
					} else if ($value === false) {
						$content = $falseBlock;
					}

					if (isset ($content)) {
						$this -> _content = str_replace ($matches[0][0], $content, $this -> _content);
					}
				} else {
					$this -> _content = str_replace ($matches[0][0], '', $this -> _content);
				}
			}
		}

		protected function evaluateIf () {
			preg_match_all ('/(\{\{if\s(\w+)\}\})(.*)\{\{\/if\}\}/sU', $this -> _content, $matches);
			if (!empty(array_filter($matches))) {
				$variable = $matches[2][0];
				$trueBlock = $matches[3][0];

				$value;
				$content;

				if (property_exists ($this, $variable)) {
					$value = $this -> $variable;
				}else if (array_key_exists ($variable, $this -> data)) {
					$value = $this -> data[$variable];
				} else if (method_exists($this, $variable)) {
					$value = $this -> $variable ();
				}

				if (isset ($value)) {
					if ($value === true) {
						$content = $trueBlock;
					}

					if (isset ($content)) {
						$this -> _content = str_replace ($matches[0][0], $content, $this -> _content);
					}
				} else {
					$this -> _content = str_replace ($matches[0][0], '', $this -> _content);
				}
			}
		}

		protected function evaluateConditionals () {
			$this -> evaluateIfElse ();
			$this -> evaluateIf ();
		}

		protected function linkFiles () {
			$scripts = [];
			$styles = [];

			// Add all CSS Stylesheets
			foreach ($this -> _styles as $style) {
				$tag = '<link rel="stylesheet"';
				if (is_string ($style)) {
					$tag .= " href=\"$style\">";
				} else if (is_array ($style)) {
					foreach ($style as $key => $value) {
						if (empty ($value)) {
							$tag .= " $key ";
						} else {
							$tag .= " $key=\"$value\" ";
						}
					}
					$tag .= '>';
				}
				$tag .= '</link>';
				array_push ($styles, $tag);
			}

			// Add all JS Scripts
			foreach ($this -> _scripts as $script) {
				$tag = '<script';
				if (is_string ($script)) {
					$tag .= " src=\"$script\">";
				} else if (is_array ($script)) {
					foreach ($script as $key => $value) {
						if (empty ($value)) {
							$tag .= " $key ";
						} else {
							$tag .= " $key=\"$value\" ";
						}
					}
					$tag .= '>';
				}
				$tag .= '</script>';
				array_push ($scripts, $tag);
			}

			$this -> _content = str_replace ('{>{scripts}<}', join ("\r\n", $scripts), $this -> _content);
			$this -> _content = str_replace ('{>{styles}<}', join ("\r\n", $styles), $this -> _content);
		}

		protected function setProperties(){
			$this -> _fullRoute = Router::fullRoute ();
			$this -> _route = Router::route ();
			$this -> _domain = Router::domain ();
			preg_match_all('/\{\{((\w*|\d))\}\}/', $this -> _content, $matches);
			if(!empty($matches)){
				foreach($matches[0] as $match){
					$matchName = trim(str_replace(array("{{", "}}"), array("", ""), $match));
					if (property_exists($this, $matchName)){
						$this -> _content = str_replace($match, $this -> $matchName, $this -> _content);
					} else if (array_key_exists($matchName, $this -> data)) {
						$this -> _content = str_replace($match, $this -> data[$matchName], $this -> _content);
					} else if(method_exists($this, $matchName)){
						$this -> _content = str_replace($match, $this -> $matchName(), $this -> _content);
					}

				}
			}
		}

		protected function includeTemplates(){
			preg_match_all('/\{\{>(\s?)(\w*|\d*)\}\}/', $this -> _content, $matches);
			if(!empty($matches)){
				foreach($matches[0] as $match){
				    $matchName = trim(str_replace(array("{{>", "}}"), array("", ""), $match));
				    if(class_exists($matchName)){
				    	$template = new $matchName();
				    	$this -> _content = str_replace($match, $template, $this -> _content);
				    }else if(($found = FileSystem::findFile('lib/components', "$matchName.html")) != null){
				    	$this -> _content = str_replace($match, file_get_contents($found), $this -> _content);
				    }
				}
			}
		}

		function __toString(){
			$this -> compile();
			return $this -> _content;
		}

		protected function includeRepeats () {
			preg_match_all('/\{\{repeat(\s)(\w*|\d*)(\sfor\s|\s)(\w*|\d*)\}\}/', $this -> _content, $matches);

			if (!empty($matches)) {
				foreach($matches[0] as $match){
					$expression = explode(" ", trim(str_replace(array("{{repeat ", "}}", " for "), array("", "", " "), $match)));
					$class = $expression[0];
					$list = $expression[1];
					$content = "";

					if (method_exists ($this, $list)) {
						$objects = $this -> $list ();
					}else if (property_exists ($this, $list)){
						$objects = $this -> $list;
					} else if (is_numeric ($list)) {
						$objects = intval ($list);
					} else if (array_key_exists($list, $this -> data)) {
						$objects = $this -> data[$list];
					} else {
						return null;
					}

					if (is_int ($objects)) {
						if (class_exists($class) === true && $class instanceof Template) {
							for ($i = 0; $i < $objects; $i++) {
								$template = new $class ();
								$template -> _index = $i;
								$template -> _count = $i + 1;
								$content .= $template;
							}
						} else if(($found = FileSystem::findFile('lib/components', "$class.html")) !== null){
							for ($i = 0; $i < $objects; $i++) {
								$temp = file_get_contents ($found);
								$temp = str_replace ('{{_index}}', $i, $temp);
								$temp = str_replace ('{{_count}}', $i + 1, $temp);
								$content .= $temp;
							}
						} else {
							throw new Exception ("Tried to instantiate a non-existent Template '$class'.",1);
						}
					} else {
						$index = 0;
						if (class_exists($class) === true && $class instanceof Template) {
							foreach ($objects as $object) {
								$template = new $class ();
								if (is_array ($object)) {
									foreach($object as $key => $value){
										$template -> $key = $value;
										$template -> _index = $index;
										$template -> _count = $index + 1;
									}
								} else {
									$template -> data[$class] = $object;
								}

								$content .= $template;
								$index++;
							}
						} else if(($found = FileSystem::findFile('lib/components', "$class.html")) !== null){
							foreach ($objects as $object){

								$template = new Template ("$class.html");
								if (is_array ($object)) {
									$template -> _index = $index;
									$template -> _count = $index + 1;
									$template -> data = $object;
								} else {
									$template -> data[$class] = $object;
								}

								$content .= strval ($template);
								$index++;
							}
						} else {
							throw new Exception ("Tried to instantiate a non-existent Template '$class'.",1);
						}
					}

					$this -> _content = str_replace ($match, $content, $this -> _content);
				}
			}
		}
	}
?>