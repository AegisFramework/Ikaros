<?php

	namespace Ikaros;

	class Text {


	    public static function capitalize (string $text): string {
		  return ucwords (strtolower ($text));
	    }

		public static function similarity (string $text1, string $text2): float {
		  similar_text ($text1, $text2, $percent);
		  return  $percent;
	    }

	    public static function suffix (string $key, string $text): string {
	        $suffix = '';
	        $position = strpos ($text, $key);
	        if($position !== false){
	            $position += strlen ($key);
	            $suffix = substr ($text, $position, strlen ($text) - $position);
	        }
	        return $suffix;
	    }

	    public static function prefix (string $key, string $text): string {
	        return strpos ($text, $key) !== false ? substr ($text, 0, $position) : '';
	    }

	    public static function affixes (string $key, string $text): array {
	        return [self::getPrefix ($string, $key), self::getSuffix ($string, $key)];
	    }

	    public static function between (string $start, string $end, string $string): string {
	        $string = ' ' . $string;
	        $ini = strpos ($string, $start);
	        if ($ini == 0) {
		        return '';
		    } else {
			    $ini += strlen ($start);
				$len = strpos ($string, $end, $ini) - $ini;
				return substr ($string, $ini, $len);
		    }
	    }

	    public static function removeTags (string $text): string {
		    return strip_tags ($text);
	    }

	    public static function friendly (string $text): string {
			$expressions = [
				'[áàâãªä]'   =>   'a',
		        '[ÁÀÂÃÄ]'    =>   'A',
		        '[ÍÌÎÏ]'     =>   'I',
		        '[íìîï]'     =>   'i',
		        '[éèêë]'     =>   'e',
		        '[ÉÈÊË]'     =>   'E',
		        '[óòôõºö]'   =>   'o',
		        '[ÓÒÔÕÖ]'    =>   'O',
		        '[úùûü]'     =>   'u',
		        '[ÚÙÛÜ]'     =>   'U',
		        'ç'          =>   'c',
		        'Ç'          =>   'C',
		        'ñ'          =>   'n',
		        'Ñ'          =>   'N',
		        '_'          =>   '-',
		        '[’‘‹›<>\']' =>   '',
		        '[“”«»„\"]'  =>   '',
		        '[\(\)\{\}\[\]]' => '',
		        '[?¿!¡#$%&^*´`~\/°\|]' => '',
		        '[,.:;]'     => '',
		        '\s'         =>   '-'
		    ];

			foreach ($expressions as $regex => $replacement) {
				$text = preg_replace ("/$regex/u", $replacement, $text);
			}

			return $text;
	    }

	}
?>