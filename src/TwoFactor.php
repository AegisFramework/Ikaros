<?php

	namespace Ikaros;

	use PragmaRX\Google2FA\Google2FA;

	class TwoFactor {

		static function secret (int $size = 32) : string {
			$google2fa = new Google2FA ();

			return $google2fa -> generateSecretKey ($size);
		}

		static function verify (string $code, string $secret, int $window = 8) : bool {
			$google2fa = new Google2FA ();
			return $google2fa -> verifyKey ($code, $secret, $window);
		}

		static function qr (string $company, string $email, string $secret) : string {
			$google2fa = (new \PragmaRX\Google2FAQRCode\Google2FA ());

			return $google2fa -> getQRCodeInline ($company, $email, $secret);
		}
	}

?>