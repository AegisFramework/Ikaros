<?php

	namespace Ikaros;

	use Exception;

	class Configuration {

		/**
		 * Collection holding the configuration once it's loaded from a file
		 *
		 * @var Collection
		 */
		private static $configuration;

		/**
		 * Load the configuration file. The file must be in a valid JSON format.
		 *
		 * @param string $file Path to the configuration file
		 *
		 * @throws Exception If the provided file does not exists
		 *
		 * @return void
		 */
		public static function load (string $file = '.conf'): void {
			if (FileSystem::exists ($file)) {
				$content = FileSystem::read ($file);
				self::$configuration = new Collection ($content);
			} else {
				throw new Exception ("The configuration file $file does not exist.", 1);
			}
		}

		/**
		 * To simplify the retrieval of values inside the configuration file, it
		 * is possible to call them as static methods of the Configuration class.
		 *
		 * For example, the following will return the value of the 'domain' key
		 * present in the file.
		 *
		 * Configuration::domain ();
		 *
		 * @param string $name Name of the static function being called
		 * @param array $arguments Arguments with which the function is being called
		 *
		 * @throws Exception If the configuration has not been loaded yet
		 *
		 * @return string The value of the key or null if it didn't exist
		 */
		public static function __callStatic (string $name, array $arguments): ?string {
			if (self::$configuration !== null) {
				if (self::$configuration -> hasKey ($name)) {
					return self::$configuration -> get ($name);
				} else {
					return null;
				}
			} else {
				throw new Exception ('Configuration is not loaded.', 1);
			}
		}
	}
?>