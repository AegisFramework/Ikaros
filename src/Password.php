<?php

	namespace Ikaros;

	class Password {

		/**
		 * Generate a random secure password of the given lenght. The generated
		 * password may contain lower and upper case letters, numbers, symbols
		 * and numbers.
		 *
		 * @param int $length The desired length for the password
		 *
		 * @return string Generated password
		 */
		public static function generate (int $length = 8): string {
			$chars = 'abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*(){}[]<>?_-=+;:,.?1234567890';
			$str = '';
			$max = strlen ($chars) - 1;

			for ($i = 0; $i < $length; $i++)
				$str .= $chars[random_int (0, $max)];

			return $str;
		}

		/**
		 * Hash a password using the default algorithm
		 *
		 * @param string $password Password to hash
		 *
		 * @return string Hashed password
		 */
		public static function hash (string $password): string {
			return password_hash ($password, PASSWORD_DEFAULT, self::getCost ());
		}

		/**
		 * Check if given password matches a hash
		 *
		 * @param string $password Password to compare
		 * @param string $hash Hash to compare password against
		 *
		 * @return bool
		 */
		public static function compare (string $password, string $hash): bool {
			return password_verify ($password, $hash);
		}

	    /**
	     * Get Computational Cost to Hash Passwords.
	     *
	     * The function hashes a test password multiple time until it gets
	     * the best cost in matter of time and security. This cost depends
	     * greatly on the machine it's running.
	     *
	     * A 10 is a Standard
	     *
	     * @return array Associative Array With The Cost.
	     */
	    private static function getCost (): array {
	        $timeTarget = 0.2;
			$cost = 9;

			do {
				$cost++;
				$start = microtime (true);
				password_hash ('test', PASSWORD_DEFAULT, ['cost' => $cost]);
				$end = microtime (true);
			} while (($end - $start) < $timeTarget);

	       return ['cost' => $cost];
	    }
	}
?>